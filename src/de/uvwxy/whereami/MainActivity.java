package de.uvwxy.whereami;

import java.text.NumberFormat;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import de.uvwxy.daisy.common.location.GPSWIFIReader;
import de.uvwxy.daisy.common.location.LocationReader.LocationResultCallback;
import de.uvwxy.daisy.common.location.LocationReader.LocationStatusCallback;
import de.uvwxy.daisy.helper.IntentTools;
import de.uvwxy.daisy.helper.IntentTools.ReturnStringCallback;
import de.uvwxy.daisy.helper.ViewTools;

public class MainActivity extends Activity {
	private static final boolean SCREENSHOT_MODE = false;

	private Context ctx = this;
	private Activity act = this;

	GPSWIFIReader gpsLoc = null;

	private ToggleButton tgnStart = null;
	private TextView tvInfo = null;
	private TextView etLatitude = null;
	private TextView etLongitude = null;
	private TextView etBearing = null;
	private TextView etHeight = null;
	private TextView etAccuracy = null;
	private Button btnShare = null;
	private Button btnMap = null;

	private Location lastLocation = null;
	private long lastScanTS = System.currentTimeMillis();

	private boolean isGPSRunning = false;
	private boolean onPauseGPSWasRunning = false;

	private LocationStatusCallback cbStatus = new LocationStatusCallback() {
		@Override
		public void status(Location l) {
			updateLocation(l);
		}
	};

	private LocationResultCallback cbResult = new LocationResultCallback() {
		@Override
		public void result(Location l) {
			updateLocation(l);
		}
	};

	private void updateLocation(Location l) {
		lastLocation = l;
		if (SCREENSHOT_MODE) {
			lastLocation.setLatitude(37.4242481234512345123451);
			lastLocation.setLongitude(-122.087529);
			lastLocation.setBearing(13.37f);
			lastLocation.setAccuracy(1.0f);
			lastLocation.setAltitude(10.56);
		}
		btnShare.setEnabled(true);
		btnMap.setEnabled(true);

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(6);
		etLatitude.setText(nf.format(l.getLatitude()) + " °");
		etLongitude.setText(nf.format(l.getLongitude()) + " °");
		etHeight.setText(String.format("%.1f", l.getAltitude()) + " m");
		etBearing.setText(String.format("%.1f", l.getBearing()) + " °");
		etAccuracy.setText(String.format("%.1f", l.getAccuracy()) + " m");
		tvInfo.setText(String.format(getString(R.string.location_update_after), (System.currentTimeMillis() - lastScanTS) / 1000f, l.getProvider()));
		lastScanTS = System.currentTimeMillis();
	}

	private void initGUI() {
		tgnStart = (ToggleButton) findViewById(R.id.tgnStart);
		//		btnStop = (Button) findViewById(R.id.btnStop);
		btnShare = (Button) findViewById(R.id.btnShare);
		btnMap = (Button) findViewById(R.id.btnMap);

		tvInfo = (TextView) findViewById(R.id.tvInfo);

		etLatitude = (TextView) findViewById(R.id.etLatitude);
		etLongitude = (TextView) findViewById(R.id.etLongitude);
		etBearing = (TextView) findViewById(R.id.etBearing);
		etHeight = (TextView) findViewById(R.id.etHeight);
		etAccuracy = (TextView) findViewById(R.id.etAccuracy);

		ViewTools.addCopyTextOnClickListner(ctx, etLatitude, getString(R.string.copied_value_to_clipboard_) + getString(R.string.latitude));
		ViewTools.addCopyTextOnClickListner(ctx, etLongitude, getString(R.string.copied_value_to_clipboard_) + getString(R.string.longitude));
		ViewTools.addCopyTextOnClickListner(ctx, etHeight, getString(R.string.copied_value_to_clipboard_) + getString(R.string.altitude));
		ViewTools.addCopyTextOnClickListner(ctx, etBearing, getString(R.string.copied_value_to_clipboard_) + getString(R.string.bearing));
		ViewTools.addCopyTextOnClickListner(ctx, etAccuracy, getString(R.string.copied_value_to_clipboard_) + getString(R.string.accuracy));

		btnShare.setEnabled(false);
		btnMap.setEnabled(false);

		tvInfo.setText(R.string.click_start);
	}

	private void initClicks() {
		tgnStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (tgnStart.isChecked()) {
					startGPS();
				} else {
					stopGPS();
				}

			}

		});

		btnShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ReturnStringCallback selection = new ReturnStringCallback() {

					@Override
					public void result(String s) {
						String url = "";
						if (s.equals("Google Maps")) {
							url = IntentTools.getGoogleMapsUrl(lastLocation.getLatitude(), lastLocation.getLongitude());
						} else if (s.equals("OpenStreetMap")) {
							url = IntentTools.getOSMMapsUrl(lastLocation.getLatitude(), lastLocation.getLongitude());
						}
						IntentTools.shareLocation(ctx, url, lastLocation.getLatitude(), lastLocation.getLongitude(), lastLocation.getAltitude(),
								lastLocation.getBearing(), lastLocation.getAccuracy(), getString(R.string.my_location), getString(R.string.lat_),
								getString(R.string.lon_), getString(R.string.alt_), getString(R.string.bear_), getString(R.string.acc_),
								getString(R.string.message_length_), getString(R.string.share_location_via));
					}

				};

				if (lastLocation != null) {
					IntentTools.userSelectString(ctx, getString(R.string.select), new String[] { "Google Maps", "OpenStreetMap" }, selection);
				} else {
					Toast.makeText(ctx, R.string.no_location, Toast.LENGTH_SHORT).show();
				}

				if (lastLocation != null) {

				}
			}
		});

		btnMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ReturnStringCallback selection = new ReturnStringCallback() {

					@Override
					public void result(String s) {
						if (s.equals("Google Maps")) {
							IntentTools.showURL(act, IntentTools.getGoogleMapsUrl(lastLocation.getLatitude(), lastLocation.getLongitude()));
						} else if (s.equals("OpenStreetMap")) {
							IntentTools.showURL(act, IntentTools.getOSMMapsUrl(lastLocation.getLatitude(), lastLocation.getLongitude()));
						}

					}

				};

				if (lastLocation != null) {
					IntentTools.userSelectString(ctx, getString(R.string.select), new String[] { "Google Maps", "OpenStreetMap" }, selection);
				} else {
					Toast.makeText(ctx, R.string.no_location, Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initGUI();
		initClicks();
	}

	@Override
	protected void onResume() {
		if (gpsLoc == null) {
			gpsLoc = new GPSWIFIReader(this, -1, -1, cbStatus, cbResult, true, true);
		}

		if (lastLocation != null) {
			updateLocation(lastLocation);
		}

		if (onPauseGPSWasRunning) {
			tgnStart.setChecked(true);
			startGPS();
			onPauseGPSWasRunning = false;
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		if (isGPSRunning) {
			onPauseGPSWasRunning = true;
		}
		tgnStart.setChecked(false);
		stopGPS();
		super.onPause();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onDestroy() {
		gpsLoc.stopReading();
		super.onDestroy();
	}

	private void startGPS() {
		gpsLoc.startReading();
		isGPSRunning = true;
		tvInfo.setText(R.string.waiting_for_fix);
	}

	private void stopGPS() {
		gpsLoc.stopReading();
		isGPSRunning = false;
		tvInfo.setText(R.string.click_start);
	}

}
